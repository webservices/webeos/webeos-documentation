# Managing a webEOS site

## Getting to the site management screen
The webEOS site management screen is available from `My sites` listing, and can be accessed by clicking on a site name or on the cogwheel settings icon.

![Screenshot](images/portal-my-sites.png)

## The webEOS site management screen
The form is relatively simple and allows to update all of the webEOS site settings, while providing guidance about the allowed values.

![Screenshot](images/portal-webeos-update.png)

The values for site category, description, owner and administrator group are synchronized with the Application Portal and their update in the form will automatically propagate them to the Application Portal.

!!! success "Change URL"
    The internal name of the site can't be changed, but its URL can. Simply edit the values in the `Served From` section: `Site URL`.

!!! warning
    When changing the EOS location from which the site contents are served, make sure that the new location is also shared with the `a:wwweos` account. [More info](../create_site#prerequisites).

!!! warning "Owner change"
    The owner property cannot be changed together with other properties on the same request.


In case you want to start using a reserved hostname for the site, please create a [SNOW Request](https://cern.service-now.com/service-portal/?id=service_element&name=eos-web-hosting) specifying the project name and the desired site name.


## Controlling access to the site

Follow the [Securing webEOS sites](../authorization/) guide for more details on how to manage access to your site.


## Using .htaccess files

An .htaccess file is a directory-level configuration file supported by Apache HTTP Server, allowing for advanced configuration of a site, such as establishing more granular (per directory) access control, URL rewrite, directory browsing, cgi execution and others. Any folder below the site's root path (such as `/eos/user/j/johnny/www`) can have an .htaccess file, however it's advisable to use this mechanism with care as it has some negative impact on site's load time.

By default the `.htaccess` files are not processed, in order to enable them make sure to enable the setting `Use .htaccess files` in the section `Site Access` of the webEOS site management screen.

### Enabling cgi execution

By default webEOS web sites don't have CGI scripts enabled. In order to allow execution of CGI scripts in a folder (e.g. `/eos/user/j/johnny/www/cgi-bin`), include the following directives in the .htaccess file placed in this directory:

```
Options +ExecCGI
SetHandler cgi-script
```

Note that these directives apply to all subfolders, therefore cgi execution will also be enabled for them. Make sure that CGI-enabled folders contain only executable scripts. HTML files won't work in these folders because everything is treated as a CGI program.

!!! warning
    The setting `Use .htaccess files` must be enabled.

### Enabling directory browsing

"Directory Browsing" can be enabled with a following directive in the .htaccess file:
```
Options +Indexes
```

The "Directory Browsing" setting will be propagated to all subdirectories below the one containing the .htaccess file. To disable it for a subdirectory use "Options -Indexes" in a corresponding .htaccess file.

Remember to remove the index file of your directory, otherwise it will be loaded and the directory browsing will not work.

!!! warning
    The setting `Use .htaccess files` must be enabled.

### Enforce HTTPS

Unlike  the previous implementation of the WebEOS infrastructure, the new infrastructure by default enforces HTTPS connections when your website is accessed.
Therefore no additional configuration is required.
Moreover any pre-existing configuration related to HTTP -> HTTPS redirection in the .htaccess files should be removed, as it will result in an infinite redirect and failure to access the website.
