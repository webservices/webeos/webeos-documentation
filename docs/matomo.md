# Web analytics

## Matomo

IT-PW-WA offers a dedicated web analytics service.

![Screenshot](images/matomo-logo.png)

Matomo, formerly Piwik, is a free and open source web analytics application and is an alternative to Google Analytics.

It provides detailed reports on your website and its visitors, including the search engines and keywords they used, the language they speak, which pages they like, the files they download and so much more. 

![Screenshot](images/matomo-example-page.png)

### GDPR

Matomo is [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation) compliant, users can opt-out of all tracking and the tracked data is anonymized. The geolocation of the visitor to a website is approximated using only the first 2 bytes.

## Web Analytics for WebEOS sites

As the owner or administrator of a website you can setup Matomo from the [webservices](https://webservices-portal.web.cern.ch/) portal.

- On the webservice-portal `My sites` list select your website to go to the site management screen
- In the section `Web Analytics` click the button `Register site for Matomo` which will register your account and will also add your website on the Matomo server 
- Javascript code will then be available for you to add to your website. This code must be inserted inside the `<head> </head>` tags on every page of the website
- Your website will be allocated a unique identification number `siteID` . The following example has a `siteID=1`

![Screenshot](images/matomo-example-JS.png)


!!! note "Deleted sites"
    Please note that in case you remove a site that is being tracked by Matomo, you can request the deletion of the Matomo registration via a [SNOW ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&fe=web-infrastructure). If you don't delete the Matomo registration this data will be kept, linked to the URL that the deleted site had, so if another site takes this URL the Matomo Web Analytics data will be linked to this new site.


### Web Analytics for Drupal sites

Details on how to configure Web Analytics for Drupal sites can be found here: <https://drupal.docs.cern.ch/web-analytics>.

## Viewing the webanalytics

### Basic statistics
The [webservices](https://webservices-portal.web.cern.ch/) portal will offer the basic monitoring information such as

- The number of unique visitors
- The number of actions that users make
- The number of pageviews

### Advanced usage 

To access to additional statistics logon to [webanalytics.web.cern.ch](https://webanalytics.web.cern.ch)
If you have registered a website with our Matomo server then you can login using your CERN SSO account (attention: click on the green button `CERN OAUTH LOGIN`)

![Screenshot](images/matomo-cern-login.png)

You will only be able to view the statistics for the sites that you own or have been access to.

Take a [Tour of the feature]( https://matomo.org/docs/matomo-tour/)

!!! warning
    Matomo's web interface could be helpful in more advanced analytics of the site. Please note however that the interface is provided as is, without any guarantee, and may change in future versions.
