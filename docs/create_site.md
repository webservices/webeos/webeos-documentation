# Creating a webEOS site

## Prerequisites

There are a few prerequisites when creating a new webEOS site:

1. Only CERN primary accounts can own websites. Therefore in order to create a site you need to be logged in to the [Web Services Portal](https://webservices-portal.web.cern.ch) with the correct account which will be set as the owner of the site. [More on site ownership](#site-owner)

2. Your site's content must be on EOS, therefore either:
    - copy the content to the EOS location of your choice (common are `/eos/project/<initial>/<projectname>/www` for project's websites and `/eos/user/<initial>/username/www` for personal websites). The `www` subpath is a recommended convention, but you can decide on a different name;
    - if the content is not yet ready, start with an empty EOS folder, initialized with a minimal `index.html` file.

    ??? note "Example of a minimal `index.html` file"
        ```html
        <!DOCTYPE html>
        <html>
        <body>
            <h1>Welcome to my site</h1>
            <p>Some very interesting content.</p>
        </body>
        </html>
        ```

    [More about EOS](#path-to-site-content-on-eos)

3. **Grant the web servers access to the EOS location**.
   In general, this means sharing the EOS folder with account `a:wwweos` (notice the prefix `a`) as _Viewer_ from [CERNBox](https://cernbox.cern.ch):
   see how to share a [personal EOS folder](https://cernbox.docs.cern.ch/advanced/web-pages/personal_website_content/)
   or a [project EOS folder](https://cernbox.docs.cern.ch/advanced/web-pages/project_website_content/).
   If share properties of the folder are not accessible from CERNBox (e.g. folders in an Experiment's EOS instance),
   [set the permission using EOS command line interface](https://eos-docs.web.cern.ch/diopside/manual/interfaces.html?highlight=acl#acls):
   in this case, the appropriate folder ACL is `u:wwweos:rx`.

## Site Creation Form

Creating a webEOS site is as easy as going to the [Web Services Portal](https://webservices-portal.web.cern.ch/webeos) and filling in the form.

![Screenshot](images/portal-webeos-creation-form.png)

!!! warning    
    Since [OTG0148791](https://cern.service-now.com/service-portal?id=outage&n=OTG0148791),
    only [CERN primary accounts](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0001066) can own websites.

!!! note
    It may take a few minutes before the site is up, due to the DNS propagation delay.

## Site Category

There are three categories for all web resources at CERN, including websites:

- `Official` - recommended for projects and teams (Departments, Groups, Sections, Projects, HEP, Experiments, Conferences, Events, Topics, Clubs, ...)
- `Personal` - recommended for personal sites (description of work, curriculum vitae, coordinates, academic qualifications, hobbies, ...)
- `Test` - recommended for testing (pre-production, experimental, ...)

By selecting the `Official` category, you commit to make an effort to maintain appropriate contents of the site, as such sites contribute to the official image of CERN. Official web sites will not be deleted automatically, but they will be transferred to your supervisor once you leave CERN.

By selecting `Personal` category, you have more freedom, but the contents must still enforce the CERN Computing Rules. Personal web sites have the same functionality as official web sites, however they will be deleted when you leave CERN and your mail account is deleted. You should not store any content that would need to persist after your departure in your personal web site.

`Test` category can be selected for test use cases. These sites are similar to the `Personal` sites but have a limited lifetime and will be automatically deleted after 6 months if their expiration date is not explicitly prolonged.

!!! success "Change Category"
    You can easily update the category of your website at any time.

For more information about how Site Category affects its lifecycle, see [chapter about lifecycle](../lifecycle).

## Site Name

Each web site registered in the CERN Web Services must have a unique name. This name is _initially_ used as the site's web address, e.g. for a name `xyzworkshop` and a subdomain `.web.cern.ch` we create a URL `https://xyzworkshop.web.cern.ch`.

!!! success "Change URL"
    The site's URL (web address) can easily be changed at any time. Although it's best to avoid URL changes as it may result in broken links once the site is referenced from other pages.

### Naming restrictions

Site names are between 4 and 63 characters long, and must be made of lowercase letters, digits and dashes ("-") only and must start and end with an alphanumeric character (e.g. 'my-name', or '123-abc').

!!! tip
    It is possible to register an unlimited number of aliases for each web sites; aliases can contain special characters and be up to 250 characters long. Aliases can be used in place of the site's original name.

#### Reserved names

!!! info

    `Reserved` in this context means that a given name can't be used without a prior approval from the cluster admins.

The Web Services Portal validates by default the name of the website during creation against a predefined list of `reserved` names. In order to bypass this limitation, create the webEOS site via its [creation form](https://webservices-portal.web.cern.ch/webeos) using another name (for example `my-project` as a site name) and create a [SNOW Request](https://cern.service-now.com/service-portal/?id=service_element&name=eos-web-hosting) asking for this limitation to be lifted off on your project.

!!! note
    The value for the site name is also used for the name of the OKD project available on webeos.cern.ch (as the infrastructure available on [webeos.cern.ch](https://webeos.cern.ch/) is used to host all the WebEOS sites). The same check is also performed against the project name. In order to lift off this limitation, please, open a [SNOW Request](https://cern.service-now.com/service-portal/?id=service_element&name=eos-web-hosting).


### Naming conventions for Official sites

There are naming conventions for web site URLs, which should be followed as much as possible, so as to provide a consistent naming and allow users to find information more easily. You should use the site URL which you think is the most descriptive for your activity, provided it follows the naming rules and that it is currently unused.

The most important rules are the following:

- Choose a name describing the activity

    In general all official names refer to an activity. This activity is usually supported by a CERN unit (department, group, section - also called organizational units). It is recommended to give names that reflect the activity rather than the full hierarchical path that reflects the CERN Unit in charge of it. Indeed, the lifetime of the unit (at least, its name) is often shorter than the activity supported.

    However, there is an obvious case where a full hierarchical path to the CERN Unit is appropriate. That is when the page is precisely the description of that Unit (e.g. mandate, people, structure of a group, section, ...). In such cases, a hierarchical alias may be created (see later).

- Choose a name neither too long, nor too short (the best length is around 10-20 characters)

- Choose names with a long life expectancy

    The risk lies in the fact that a page may be referenced from multiple other pages, either on-site of off-site. If the URL changes, the links will become obsolete (broken links).

#### Examples of naming

- I need a site for the rugby club in the "association du personnel" :  `https://club-rugby.web.cern.ch`
- I need a site for the discussion on a topic of interest in my experiment :  `https://atlas-topic.web.cern.ch`
- I need a site for a documentation of a web application `my-app`: `https://my-app.docs.cern.ch`

## Site Subdomain

The subdomain allows you to customize your site's URL. The currently allowed subdomains are:

- `.web.cern.ch`
- `.docs.cern.ch`

We recommend using the `.docs.cern.ch` for documentation sites.

!!! note ".webtest.cern.ch subdomain"
    A third subdomain, *.webtest.cern.ch* subdomain, is needed to facilitate the migration of some WebEOS sites which remained on the old VM-based infrastructure. We're automatically migrating these sites to the webeos cluster, but first in PREVIEW mode, where the sites receive a .webtest.cern.ch domain, so that the original production site and the preview site can coexist for some period of time allowing the owners to validate their preview sites. During this validation time, they may want to check the site configuration in the Web Services Portal and therefore it's important to correctly show the site URL. **Please, if this is not your case, DO NOT choose *.webtest.cern.ch* subdomain**.

## Site Description

The description of your web site is mandatory and should reflect the site's purpose.

## Path to Site Content on EOS

The content of the website must be stored somewhere on the [CERN EOS](https://cern.service-now.com/service-portal/?id=service_element&name=eos-service) storage ([learn more about EOS](https://eos-web.web.cern.ch/eos-web/)). There are EOS instances dedicated to the experiments (EOS for Physics), and general purpose instances for projects (`EOSPROJECT`) and users (`EOSUSER`). Majority of the websites store their content on the `EOSPROJECT` and `EOSUSER` instances, which are available from `lxplus`:

- `/eos/project/<initial>/<projectname>`
- `/eos/user/<initial>/<username>`

!!! important
    The EOS Path of your site needs to be the path to the directory which contains the *index* file. For example, if you have several sub-folders, and your site's contents are under `mysite`, the path would be: `/eos/user/c/ctojeiro/www/mysite/`.

The content of `Official` websites should be stored on the `EOSPROJECT` instance or on one of the dedicated experiment instances for `ATLAS`, `ALICE`, `CMS` and `LHCb`, and the content of `Personal` and `Test` sites on `EOSUSER` instance. The content can be easily managed via the [CERNBox web interface](https://cernbox.cern.ch/). See [CERNBox Docs](https://cernbox.docs.cern.ch) for details.

!!! note
    The EOS path must start with one of the following:
    `/eos/user/`, `/eos/project/`, `/eos/atlas/`, `/eos/ams/`, `/eos/cms/`, `/eos/experiment/`, `/eos/lhcb/`, `/eos/media/`, `/eos/theory/`, `/eos/workspace/`, `/eos/web/` . In particular these formats for `EOSUSER`: `/eos/home-<letter>/` and `EOSPROJECT`: `/eos/project-<letter>/` are not supported.

## Site Owner

All web sites must be owned by active CERN primary accounts. The account used to register the Web site will determine the owner of the site. This person will be considered as the person responsible for its content (especially because the administration of the site and the creation of web pages can be delegated).

Secondary and Service accounts cannot be owners of CERN websites. Only primary accounts can.

It is possible to change a web site's owner later on, from the [management form](../manage_site/#getting-to-the-site-management-screen).
The owner may also [set an administrator group](../manage_site.md#the-webeos-site-management-screen) to allow multiple users to manage the site.
The administrator group may include secondary and service accounts.

Any user can have as many Web Sites as necessary, provided the site names are unique CERN-wide. To obtain more Web Sites, just request them as many times as necessary.

!!! warning
    All `Personal` web sites of a user will be **automatically deleted** if he/she leaves CERN and becomes unknown in the HR (Human Resources) database or if their account in the computer center is deleted. If a site with category `Personal` should be preserved, it is the responsibility of the site owner to change the site's category to `Official` before leaving CERN. This can be done from the [management form](../manage_site/#getting-to-the-site-management-screen).
    In addition **site owner must ensure that the site's content is moved out of their personal EOS area**, which is also deleted following the user's end of contract.
