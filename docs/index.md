# Website Hosting for EOS / CERNBox

The following documentation describes how to use CERN's WebEOS Service for publishing content stored on EOS on the web. The Service infrastructure is built on the OpenShift Kubernetes platform ([OKD4](https://docs.okd.io/latest)).

This service is intended to facilitate publishing of websites in the scope of official projects, collaborations and professional activities and their integration with CERN Authentication and Authorization.

## Is this the documentation I'm looking for?

Note that some services use separate, dedicated instances of OpenShift and have their own documentation:

- dedicated hosting for documentation sites:
    - [Documentation sites/GitLab Pages](https://how-to.docs.cern.ch) (files are stored in a GitLab repository)
- application hosting (Platform-as-a-Service):
    - [PaaS](https://paas.docs.cern.ch)
- application templates (provision an instance of a popular application in a few clicks):
    - [Grafana](https://grafana.docs.cern.ch)
    - [Wordpress](https://wordpress.docs.cern.ch)
    - (more to come)

## Overview

Here is an overview of the features of the WebEOS Service:

- Sites's URL look like <https://sitename.web.cern.ch>.
- Site management is available through the [Web Services Portal](https://webservices-portal.web.cern.ch).
- Site access (concerns only non-public sites) is controlled by [the new SSO system](https://auth.docs.cern.ch/). More information here: <https://webeos.docs.cern.ch/authorization>
- Sites are served by `Apache HTTP Server Version 2.4`.
- The sites' contents are on `EOS`, in directories designated and managed by site owners. Please refer to [this manual](https://cernbox.docs.cern.ch/advanced/web-pages/) for instructions on how to enable an EOS directory as site's content source.
