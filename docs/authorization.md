# Securing webEOS sites

## Introduction
WebEOS sites can be configured with various access options, allowing a significant level of control on who can access the site, and which parts (directories) of the site they can access. In particular site administrators can decide to make certain parts of the site available without authentication while restricting other parts.

The access (authorization) settings for webEOS sites are managed both by the Web Services Portal and by the Application Portal, where each webEOS site is registered:

- The Web Services Portal manages the global configuration such as guest access and per-directory access
![Screenshot](images/portal-webeos-authorization.png)

- The Application Portal manages the detailed permissions, that is which groups have access to the site
![Screenshot](images/application-portal-authorization.png)

!!! note
    The registration of sites in the Application Portal is done automatically at site creation time. The site counterpart in the Application Portal is named according to the schema `webframeworks-webeos-<sitename>`. For example a webEOS site `webservices-demo` is named `webframeworks-webeos-webservices-demo` in the Application Portal.

**By default, only CERN and eduGAIN users have access to a webEOS site.** This is achieved by an automatic creation of a role called `default-role` at the moment of site registration in the Application Portal.

This default access setup can be modified as described below.

## Guest Access

This setting is available in the webEOS management screen. By default it is disabled so that the site uses the CERN SSO and has a reasonable level of access protection, as recommended by the CERN Security Team. If the setting is enabled access to the site is allowed for unauthenticated users (`guest` users), no SSO login screen will appear.

## Manage roles via the Application Portal

In the Web Services Portal webEOS management screen, click on `Manage roles via the Application Portal`.

![Screenshot](images/portal-webeos-general-info.png)

From the [Application portal](https://application-portal.web.cern.ch/), you can manage the detailed permissions of your site.

## Providing access to a group / application role

Access the application in the Application Portal following [Manage roles via the Application Portal](#manage-roles-via-the-application-portal) guide.

In order to grant access to a group or groups, you have to add an `Application role` to the site and map this role to **one or more** groups by following the steps mentioned [here](https://auth.docs.cern.ch/applications/role-based-permissions/).

You can use the role to protect the entire site in which case there is nothing else to do (just make sure that `Guest Access` is disabled!)

## Controlling access per directory

Alternatively you can use the role to protect a `subsite` of your site, that is a specific EOS subdirectory. To achieve that specify the role in an `.htaccess` file placed in the EOS directory which you want to protect, like this:

```
AuthType openid-connect
Require claim cern_roles:<role_identifier>
```

!!! warning
    The setting `Use .htaccess files` must be enabled.

## Disabling authentication per directory

Even if the site is protected as such, there is a possibility to remove protection for a specific `subsite` (EOS subdirectory) thus allowing unauthenticated access to that part of the site. To achieve this add the following `.htaccess` file at the level which you wish to open to the public.

```
AuthType None
Require all granted
```

!!! warning
    The setting `Use .htaccess files` must be enabled.

## Enabling CERN Intranet access restriction

CERN Intranet access restriction can be enabled with a following directive in the .htaccess file:

```
# The list of all CERN networks. See https://landb.cern.ch/landb/portal/cernNetwork
require ip 2001:1458::/32 2001:1459::/32 FD01:1458::/32 FD01:1459::/32 10.0.0.0/8 100.64.0.0/10 128.141.0.0/16 128.142.0.0/16 137.138.0.0/16 172.16.0.0/12 188.184.0.0/15 192.16.155.0/24 192.16.165.0/24 192.91.242.0/24 192.168.0.0/16 194.12.128.0/18
```

Note that you can:

*    combine user authentication with access restriction by using [RequireAll](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html#requireall) (logical AND)
*    allow one of the multiple restrictions by using [RequireAny](https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html#requireany) (logical OR).

!!! warning
    The setting `Use .htaccess files` must be enabled.

## Common scenarios

### The whole site accessible to unauthenticated users

- Enable the option `Guest Access` in the Web Services Portal webEOS management screen.

### The whole site accessible to specific groups

- Disable the option `Guest Access` in the Web Services Portal webEOS management screen.
- Access the application in the Application Portal following [Manage roles via the Application Portal](#manage-roles-via-the-application-portal) guide.
- Modify the `default-role` role by adding one or more groups:

    A. Click the orange icon to `Edit role details`.

    ![Screenshot](images/application_portal-roles_overview_edit.png)

    ---

    B. Uncheck the checkbox <input type="checkbox"> `This role applies to all authenticated users` and click `Submit`.

    ![Screenshot](images/application_portal-edit_role.png)

    ---

    C. Then, click the green icon to `Assign role to groups`.

    ![Screenshot](images/application_portal-roles_overview_add_group.png)

    ---

    D. Search and select the group that you want to allow access.

    ![Screenshot](images/application_portal-linked_groups.png)

    ---

    After these steps, the selected group(s) is/are linked with the `default-role`. Make sure that the `default-role` is marked as `Required`.

    !!! warning
        Please verify if the `Level Of Assurance` defined for the role is still accurate in respect to the group you used. By default it is set to `CERN accounts and EduGAIN users`, but if the group you chose to grant access to contains lightweight accounts, you need to change this setting to the lowest level `Social Accounts`, to allow the lightweight accounts to access the site.

### The whole site accessible to `CERN` users only

- Disable the option `Guest Access` in the Web Services Portal webEOS management screen.
- Access the application in the Application Portal following [Manage roles via the Application Portal](#manage-roles-via-the-application-portal) guide.
- Modify the `default-role` role:

    A. Click the orange icon to `Edit role details`.

    ![Screenshot](images/application_portal-roles_overview_edit.png)

    ---

    B. Make sure that the checkboxes <input type="checkbox"> `This role is required to access my application` and <input type="checkbox"> `This role applies to all authenticated users` are checked. Set the `Minimum Level of Assurance` to `CERN (highest)` and click `Submit`.

    ![Screenshot](images/application_portal-roles_edit.png)

    ---

### Root of the site accessible to unauthenticated users and some directories accessible to `CERN` users

In the Web Services Portal webEOS management screen:

- Enable the option `Guest Access`
- Enable the option `Use .htaccess files`

Then, access the application in the Application Portal following [Manage roles via the Application Portal](#manage-roles-via-the-application-portal) guide:

- Modify the `default-role` role:

    A. Click the orange icon to `Edit role details`.

    ![Screenshot](images/application_portal-roles_overview_edit.png)

    ---

    B. Make sure that the checkboxes <input type="checkbox"> `This role is required to access my application` and <input type="checkbox"> `This role applies to all authenticated users` are checked. Set the `Minimum Level of Assurance` to `CERN (highest)` and click `Submit`.

    ![Screenshot](images/application_portal-roles_edit.png)

    ---

- Use that role in the .htaccess file, placed in the EOS directory which you want to protect, like this:

    ```bash
    AuthType openid-connect
    Require claim cern_roles:default-role
    ```

### Root of the site accessible to unauthenticated users and some directories protected

In the Web Services Portal webEOS management screen:

- Enable the option `Guest Access`
- Enable the option `Use .htaccess files`

Then, access the application in the Application Portal following [Manage roles via the Application Portal](#manage-roles-via-the-application-portal) guide:

- Delete the role `default-role`
- Create a new `Required` role and associate it with one or more groups
- Use that role in the .htaccess file, placed in the EOS directory which you want to protect, like this:

```bash
AuthType openid-connect
Require claim cern_roles:<role_identifier>
```

!!! note
    If different folders need to be available to different groups, create separate roles in the Application Portal: each role with the target group assigned, so that you can refer to these individual roles in each of the .htaccess files.

### Root of the site restricted to a group of users and some directories public

In the Web Services Portal webEOS management screen:

- Disable the option `Guest Access`
- Enable the option `Use .htaccess files`

Then, access the application in the Application Portal following [Manage roles via the Application Portal](#manage-roles-via-the-application-portal) guide:

- Modify the existing role `default-role` by adding one or more groups.

- In the top EOS folder (root) of the site, in the `.htaccess` file, indicate that `default-role` is necessary:

```bash
AuthType openid-connect
Require claim cern_roles:default-role
```

- In the EOS directory which you want to be public, place an `.htaccess` file with the following directives:

```bash
AuthType None
Require all granted
```

## Advanced topics

### How to logout users from a webeos site

You can kill the session from your application by redirecting the user to `/oidc/callback` with a parameter named `logout`. Example:

```
<a href="/oidc/callback?logout=https://<site URL>/desired_landing_page_after_logout.html">Logout</a>
```

### Using a specific SSO implementation in your application

Single-page applications or specific application frameworks may want to use their own libraries to implement SSO, instead of the Apache authentication module.
A SSO application registration is automatically created in the [Application Portal](https://application-portal.web.cern.ch/) for use by the Apache authentication module, but it should not be used for other SSO libraries.

In order to use other SSO libraries, create a second registration in the [Application Portal](https://application-portal.web.cern.ch/).

### Technical Network support

To make your application "visible" from CERN's Technical Network (*TN Ingress*), 
please open a ticket to the [EOS Web Hosting Service](https://cern.service-now.com/service-portal?id=service_element&name=eos-web-hosting).
After approval by the security team (see ["Security Checklist for Developers"](https://security.web.cern.ch/recommendations/en/checklist_for_coders.shtml)), the feature will be enabled for your website.

Note that generally speaking, TN Ingress and Internet Ingress are *mutually exclusive* - speacial exception may be granted by the security team.
