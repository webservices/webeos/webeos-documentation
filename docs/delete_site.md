# Deleting a webEOS site

You can delete a webEOS site from the webEOS site management screen:

![Screenshot](images/portal-webeos-delete.png)

A confirmation screen will appear before the site is actually deleted.

Notice that the deletion concerns only the site configuration in the hosting system, therefore the web site's content (stored on EOS) will not be impacted.

!!! note "Matomo Web Analytics"
    Please note that in case you have registered your site for Web Analytics and you don't need the analytics data anymore, you can request the deletion of the Matomo registration via a [SNOW ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&fe=web-infrastructure). If you don't delete the Matomo registration this data will be kept, linked to the URL that the deleted site had, so if another site takes this URL the Matomo Web Analytics data will be linked to this new site.
