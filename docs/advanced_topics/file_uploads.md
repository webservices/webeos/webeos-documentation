# Enable file uploads for a webEOS site

## Important security considerations

Special care is required when designing a web site with file upload functionality (or more generally, any functionality that results in writing data to a file).

!!! warning
    If a web site allows public file uploads from Internet, it must not make the uploaded files publicly readable or disclose whether a given file is already present. (As this could allow distribution of inappropriate, copyrighted or malicious material)

Even for non-public web sites [behind SSO](../authorization.md), setting up a WebEOS site with functionality that writes to files or folders causes security concerns.
Please follow carefully the recommendations in this page to minimize the security risks associated with write functionality.


## Recommended solutions

### Upload files via CernBox

Enabling users to upload files via CernBox is the recommended solution. Create a folder in CernBox and then create `Public link` for that folder with `Uploader` role. See [related documentation](https://cernbox.docs.cern.ch/web/sharing/public-share/). Copy the link that is created and refer to it in your webEOS site.

### Deploy applications with file upload functionality to PaaS instead

[PaaS](https://paas.docs.cern.ch/) application hosting allows for cleaner separation between application "code" and "data" (in contrast to WebEOS that hosts everything is under the same filesystem, EOS).

This capability enables applications with built-in upload functionality to more safely implement file upload functionality.

The application must still follow the [recommendations](#important-security-considerations) above.

## Setting up writable folders on EOS (not recommended)

!!! warning
    This solution is highly discouraged for security reasons. It aims at minimizing risks associated with a web site writing to EOS but cannot eliminate them. It must only be considered to preserve the functionality of existing _Official_ websites (not _Personal_). Whenever possible, [upload files via CernBox](#upload-files-via-cernbox) or [move to PaaS](#deploy-applications-with-file-upload-functionality-to-paas-instead). 

In exceptional cases when CernBox cannot be used to upload folders such as upstream applications with integrated upload function and applications using file-based databases, there are a few guidelines that should be followed:

1. Select an EOS upload folder: It is required that the folder that the files are stored is outside of the root folder of your site. For example, if the root folder of your site is `/eos/project/<initial>/project_name/www`, the upload folder could be set to `/eos/project/<initial>/project_name/upload_folder`.

2. Grant the web servers writable access to the EOS upload folder: in general, this means sharing the EOS upload folder (e.g. `/eos/project/<initial>/project_name/upload_folder`) with account `a:wwweos` (notice the prefix `a`) as _Editor_ from [CERNBox](https://cernbox.cern.ch):
   see how to share a [personal EOS folder](https://cernbox.docs.cern.ch/advanced/web-pages/personal_website_content/)
   or a [project EOS folder](https://cernbox.docs.cern.ch/advanced/web-pages/project_website_content/).
   If share properties of the folder are not accessible from CERNBox (e.g. folders in an Experiment's EOS instance),
   [set the permission using EOS command line interface](https://eos-docs.web.cern.ch/diopside/manual/interfaces.html?highlight=acl#acls):
   in this case, the appropriate folder ACL is `u:wwweos:rwx+d`.

3. Enable the setting `Use .htaccess files` in the section `Site Access` of [the webEOS site management screen](manage_site.md).

4. Create `.htaccess`: Disable directory indexes and script exection using `.htaccess`. The following `.htaccess` must be placed in the upload folder (e.g. `/eos/project/<initial>/project_name/upload_folder`):

```
# Don't list directory contents
IndexIgnore *

# Disable script execution
SetHandler None

# Disable directory indexes
Options -Indexes -ExecCGI
```

5. Create an subfolder in the upload folder and upload files in the that folder. For example, if the upload folder is `/eos/project/<initial>/project_name/upload_folder`, upload the actual files in `/eos/project/<initial>/project_name/upload_folder/files` to avoid the possibility of overwriting the `.htaccess`.

6. Develop functions to validate the uploaded file and ensure that only expected files are being uploaded. Verify that the following properties for the file that is uploaded by the user are met:

* Check for type of file upload and deny uploading files with other files than the expected ones (e.g. upload only images).
* Never upload an `.htaccess` file.
* Limit file size.

An example of how the EOS directory structure should look like:

```
eos/project/<initial>/project_name/
└───upload_folder
│   │   .htaccess (see step 3)
│   │
│   └───files (users' files)
│       │   image1.png
│       │   ...
│
└───www (root folder - site's content)
    │   index.php
    |   ...
```

