# Websites lifecycle policy

Each website gets automatically registered in the [CERN Application Portal](https://application-portal.web.cern.ch/) as an application with a name `webframeworks-webeos-<sitename>`.

The [Application Portal](https://application-portal.web.cern.ch/) manages the lifecycle of all web resources, including websites, to ensure that information published is maintained by an active user. It directly controls the metadata and lifecycle of the website: ownership, delegation of administrative permissions, categorization as a "Test", "Personal" or "Official" resource and deletion of the site. Changes done to this application in the Application Portal are propagated to the site.

!!! warning
    Deleting the application in the Application Portal also permanently deletes the site (but not its contents on EOS).

The lifecycle of a website depends on its category:

- `Official` - recommended for projects and teams (Departments, Groups, Sections, Projects, HEP, Experiments, Conferences, Events, Topics, Clubs, ...):
    - Automatically reassigned to team leader or supervisor 2 months after owner leaves CERN

- `Personal` - recommended for personal sites (description of work, curriculum vitae, coordinates, academic qualifications, hobbies, ...):

    - Blocked 2 months after the owner leaves CERN. That means the website still exists, but it will be offline
    - Deleted 6 months after the owner leaves
        During this grace period, if justified, the website can be reactivated and category changed. Please contact [Service Desk](mailto:service-desk@cern.ch)

- `Test` - recommended for testing (pre-production, experimental, ...):

    - Blocked 6 months after creation (this date can be extended) or 2 months after the owner leaves CERN
    - Deleted 4 months after the blocking date
