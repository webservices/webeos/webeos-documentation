# Help, support and privacy

## Service Status

Check the [IT Status Board](https://cern.service-now.com/service-portal?id=service_status_board) to see whether any incident is ongoing.

## Dedicated Discourse Topic

There is [an area on Discourse in the `Web hosting and development` category](https://discourse.web.cern.ch/c/web-hosting-development/webeos-sites/54)
 dedicated to exchange on problems and resolutions for sites hosted on the new webEOS infrastructure. 
 Please consult the existing topics which might already point you in the right direction. You may also post your question there. 

## Report an issue 

To report an issue or ask for assistance, we invite you to open a ServiceNow ticket directly with us by using [this form](https://cern.service-now.com/service-portal/?id=service_element&name=eos-web-hosting). 

## Data privacy

The use of the WebEOS web hosting service is subject to the [Privacy Notice for Web Hosting Services](https://cern.service-now.com/service-portal?id=privacy_policy&se=PaaS-Web-App&notice=web-hosting).
